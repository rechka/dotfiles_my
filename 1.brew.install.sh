#!/bin/bash
##
# Homebrew: The Missing Package Manager for OS X (http://brew.sh)
# Standard install: ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
#
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

export HOMEBREW_CASK_OPTS="--appdir=/Applications --no-quarantine --verbose"

brew install mas
read -p "Sign-in AppStore and press enter to continue"

until brew bundle check --file=~/.sys-config/Brewfile; do brew bundle install --file=~/.sys-config/Brewfile; done;

brew tap 'caskroom/drivers'
brew cask install sonos
