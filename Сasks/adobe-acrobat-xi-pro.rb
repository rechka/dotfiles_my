cask 'adobe-acrobat-xi-pro' do
  version '11' 
  sha256 :no_check # required as upstream package is updated in-place

  url "https://trials2.adobe.com/AdobeProducts/APRO/11_0_07/osx10/AcrobatPro_11_Web_WWMUI.dmg",
      user_agent: :fake,
      cookies:    { 'MM_TRIALS' => '1234' }
  name 'Adobe Acrobat Pro XI'
  homepage 'https://acrobat.adobe.com/us/en/acrobat/pdf-reader.html'

  pkg 'Adobe Acrobat XI/Adobe Acrobat XI Pro Installer.pkg'

  uninstall pkgutil:   [#needs update
                         'com.adobe.acrobat.DC.*',
                         'com.adobe.PDApp.AdobeApplicationManager.installer.pkg',
                         'com.adobe.AcroServicesUpdater',
                         'com.adobe.armdc.app.pkg',
                       ],
            launchctl: [#needs update
                         'Adobe_Genuine_Software_Integrity_Service',
                         'com.adobe.AAM.Startup-1.0',
                         'com.adobe.AAM.Updater-1.0',
                         'com.adobe.ARMDC.Communicator',
                         'com.adobe.ARMDC.SMJobBlessHelper',
                         'com.adobe.ARMDCHelper.cc24aef4a1b90ed56a725c38014c95072f92651fb65e1bf9c8e43c37a23d420d',
                         'com.adobe.agsservice',
                       ],
            delete:    '/Applications/Adobe Acrobat DC/'#needs update

  zap trash: [#needs update
               '~/Library/Application Support/Adobe/Acrobat/',
               '~/Library/Caches/Acrobat',
               '~/Library/Caches/com.adobe.Acrobat.Pro',
               '~/Library/Preferences/Adobe/Acrobat/',
               '~/Library/Preferences/com.adobe.Acrobat.Pro.plist',
             ]
end