#!/usr/bin/sh

touch ~/.hushlogin

# Install oh-my-zsh
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
#syntax highlighting
#brew installed zsh-syntax-highlighting

# Setup ~/.zshrc
# maybe look at https://gist.github.com/saetia/2764210/raw/ef18e4013ed7d11e400527268bfaff0b7b0e4a70/.zshrc
#cp ~/.sys-config/.zshrc ~/.zshrc
echo export HOMEBREW_CASK_OPTS=\"--appdir=/Applications --no-quarantine --verbose\" >> ~/.zshrc

# Run ZSH so that everything intializes
zsh

# Change user shell to zsh
#chsh #no need - automatic

# PENDING